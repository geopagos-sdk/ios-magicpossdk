//
//  MPOSService.h
//  magic-pos-demo
//
//  Created by MAGICPOS TEAM on 11/19/13.
//  Copyright (c) 2019 MAGICPOS TEAM. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CBPeripheral;

typedef NS_ENUM(NSInteger, PosType) {
    PosType_AUDIO,
    PosType_BLUETOOTH
};

typedef NS_ENUM(NSInteger, UpdateInformationResult) {
    UpdateInformationResult_UPDATE_SUCCESS,
    UpdateInformationResult_UPDATE_FAIL,
    UpdateInformationResult_UPDATE_PACKET_VEFIRY_ERROR,
    UpdateInformationResult_UPDATE_PACKET_LEN_ERROR,
    UpdateInformationResult_UPDATE_LOWPOWER,
    UpdateInformationResult_UPDATING
};

typedef NS_ENUM(NSInteger, DoPollCardResult)
{
    DoPollCardResult_NONE,
    DoPollCardResult_MCR,
    DoPollCardResult_ICC,
    DoPollCardResult_BAD_SWIPE,
    DoPollCardResult_NO_RESPONSE,
    DoPollCardResult_NOT_ICC,
    DoPollCardResult_NO_UPDATE_WORK_KEY,
    DoPollCardResult_NFC_ONLINE,   // add 20190715
    DoPollCardResult_NFC_OFFLINE,
    DoPollCardResult_NFC_DECLINED,
};

typedef NS_ENUM(NSInteger, EmvOption)
{
    EmvOption_START, EmvOption_START_WITH_FORCE_ONLINE
};

typedef NS_ENUM(NSInteger, Error)
{
    Error_TIMEOUT,
    Error_MAC_ERROR,
    Error_CMD_NOT_AVAILABLE,
    Error_DEVICE_RESET,
    Error_UNKNOWN,
    Error_DEVICE_BUSY,
    Error_INPUT_OUT_OF_RANGE,
    Error_INPUT_INVALID_FORMAT,
    Error_INPUT_ZERO_VALUES,
    Error_INPUT_INVALID,
    Error_CASHBACK_NOT_SUPPORTED,
    Error_CRC_ERROR,
    Error_COMM_ERROR,
    Error_CMD_TIMEOUT,
    Error_WR_DATA_ERROR,
    Error_EMV_APP_CFG_ERROR,
    Error_EMV_CAPK_CFG_ERROR,
    Error_APDU_ERROR,
    Error_ICC_ONLINE_TIMEOUT,
    Error_AMOUNT_OUT_OF_LIMIT,
    Error_DIGITS_UNAVAILABLE,
    Error_TRANS_KEY_ERROR,
    Error_EMV_AID_MISSING_ERROR,
    Error_MULTIPLE_CARD,
    Error_CTL_LIMIT_EXCEED
};

typedef NS_ENUM(NSInteger, DHError)
{
    DHError_TIMEOUT,
    DHError_MAC_ERROR,
    DHError_CMD_NOT_AVAILABLE,
    DHError_DEVICE_RESET,
    DHError_UNKNOWN,
    DHError_DEVICE_BUSY,
    DHError_INPUT_OUT_OF_RANGE,
    DHError_INPUT_INVALID_FORMAT,
    DHError_INPUT_ZERO_VALUES,
    DHError_INPUT_INVALID,
    DHError_CASHBACK_NOT_SUPPORTED,
    DHError_CRC_ERROR,
    DHError_COMM_ERROR,
    DHError_CMD_TIMEOUT,
    DHError_WR_DATA_ERROR,
    DHError_EMV_APP_CFG_ERROR,
    DHError_EMV_CAPK_CFG_ERROR,
    DHError_APDU_ERROR,
    DHError_ICC_ONLINE_TIMEOUT,
    DHError_AMOUNT_OUT_OF_LIMIT,
    DHError_DIGITS_UNAVAILABLE,
    DHError_TRANS_KEY_ERROR,
    DHError_EMV_AID_MISSING_ERROR
};


typedef NS_ENUM(NSInteger, Display)
{
    Display_TRY_ANOTHER_INTERFACE,
    Display_PLEASE_WAIT,
    Display_REMOVE_CARD,
    Display_CLEAR_DISPLAY_MSG,
    Display_PROCESSING,
    Display_TRANSACTION_TERMINATED,
    Display_PIN_OK,
    Display_INPUT_PIN_ING,
    Display_MAG_TO_ICC_TRADE,
    Display_INPUT_OFFLINE_PIN_ONLY,
    Display_CARD_REMOVED,
    Display_MSR_DATA_READY,
    Display_QPOS_MEMORY_OVERFLOW,
    Display_TRY_AGAIN,
    Display_TRY_ANOTHER_CARD,
    Display_READING_CARD,
    Display_PLS_SEE_PHONE
    
};

typedef NS_ENUM(NSInteger, TransactionResult) {
    TransactionResult_APPROVED,
    TransactionResult_TERMINATED,
    TransactionResult_DECLINED,
    TransactionResult_CANCEL,
    TransactionResult_CAPK_FAIL,
    TransactionResult_NOT_ICC,
    TransactionResult_SELECT_APP_FAIL,
    TransactionResult_DEVICE_ERROR,
    TransactionResult_CARD_NOT_SUPPORTED,
    TransactionResult_MISSING_MANDATORY_DATA,
    TransactionResult_CARD_BLOCKED_OR_NO_EMV_APPS,
    TransactionResult_INVALID_ICC_DATA,
    TransactionResult_FALLBACK,
    TransactionResult_NFC_TERMINATED,
    TransactionResult_TRADE_LOG_FULL
    
};

typedef NS_ENUM(NSInteger, TransactionType) {
    TransactionType_GOODS, // 货物
    TransactionType_SERVICES, // 服务
    TransactionType_CASH,//现金
    TransactionType_CASHBACK, // 退货 返现
    TransactionType_INQUIRY, // 查询
    TransactionType_TRANSFER, // 转账
    TransactionType_ADMIN,//管理
    TransactionType_CASHDEPOSIT,//存款
    TransactionType_PAYMENT,// 付款 支付
    //add 2019-04-02
    TransactionType_PBOCLOG,//        0x0A            /*PBOC日志(电子现金日志)*/
    TransactionType_SALE,//           0x0B            /*消费*/
    TransactionType_PREAUTH,//        0x0C            /*预授权*/
    TransactionType_ECQ_DESIGNATED_LOAD,//        0x10                /*电子现金Q指定账户圈存*/
    TransactionType_ECQ_UNDESIGNATED_LOAD,//    0x11                /*电子现金费非指定账户圈存*/
    TransactionType_ECQ_CASH_LOAD,//    0x12    /*电子现金费现金圈存*/
    TransactionType_ECQ_CASH_LOAD_VOID,//            0x13                /*电子现金圈存撤销*/
    TransactionType_ECQ_INQUIRE_LOG,//    0x0A    /*电子现金日志(和PBOC日志一样)*/
    TransactionType_REFUND,
    TransactionType_UPDATE_PIN,
    TransactionType_CANCEL
};


typedef NS_ENUM(NSInteger, CardTradeMode) {
    CardTradeMode_ONLY_INSERT_CARD,
    CardTradeMode_ONLY_SWIPE_CARD,
    CardTradeMode_SWIPE_INSERT_CARD,
    CardTradeMode_UNALLOWED_LOW_TRADE,
    CardTradeMode_SWIPE_TAP_INSERT_CARD,// add 20190715
    CardTradeMode_SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE,
    CardTradeMode_ONLY_TAP_CARD,
    CardTradeMode_SWIPE_TAP_INSERT_CARD_NOTUP
};

@protocol MPOSServiceListener<NSObject>

@optional


-(void) onRequestWaitingUser;
-(void) onMposIdResult: (NSDictionary*)posId;
-(void) onMposInfoResult: (NSDictionary*)posInfoData;
-(void) onDoPollCardResult: (DoPollCardResult)result DecodeData:(NSDictionary*)decodeData;
-(void) onRequestSetAmountMagic;
-(void) onRequestSelectEmvAppMagic: (NSArray*)appList;
-(void) onRequestIsServerConnectedMagic;
-(void) onRequestFinalConfirmMagic;
-(void) onRequestOnlineProcessMagic: (NSString*) tlv;
-(void) onRequestTimeMagic;
-(void) onRequestTransactionResult: (TransactionResult)transactionResult;
-(void) onRequestTransactionResultAndTotalCount:(TransactionResult)transactionResult totalCount:(NSInteger)count;
-(void) onRequestTransactionLogMagic: (NSString*)tlv;
-(void) onRequestBatchData: (NSString*)tlv;
-(void) onRequestMposConnected;
-(void) onRequestMposDisconnected;
-(void) onRequestNoMposDetected;
-(void) onErrorMagic: (Error)errorState;//pls del this Delegate
-(void) onRequestDisplayMagic: (Display)displayMsg;
-(void) onRequestUpdateWorkKeyMagicResult:(UpdateInformationResult)updateInformationResult;
-(void) onRequestGetCardNoMagicResult:(NSString *)result;
-(void) onRequestSignatureMagicResult:(NSData *)result;
-(void) onReturnReversalData: (NSString*)tlv;
-(void) onReturnGetPinResult:(NSDictionary*)decodeData;
-(void) onDoGetRandomNumberResult: (NSString *)randomNum;
//add icc apdu 2019-03-11
-(void) onReturnPowerOnIccResult:(BOOL) isSuccess  KSN:(NSString *) ksn ATR:(NSString *)atr ATRLen:(NSInteger)atrLen;
-(void) onReturnPowerOffIccResult:(BOOL) isSuccess;
-(void) onReturnApduResult:(BOOL)isSuccess APDU:(NSString *)apdu APDU_Len:(NSInteger) apduLen;

//add set the sleep time 2019-03-25
-(void)onReturnSetSleepTimeResult:(BOOL)isSuccess;
//add  2019-04-02
-(void)onRequestCalculateMac:(NSString *)calMacString;
//add 2019-04-11
-(void)onReturnCustomConfigResult:(BOOL)isSuccess config:(NSString*)resutl;

-(void) onRequestPinEntry;
-(void) onReturnSetMasterKeyResult: (BOOL)isSuccess;

-(void) onReturnBatchSendAPDUResult:(NSDictionary *)apduResponses;
-(void) onReturniccCashBack: (NSDictionary*)result;
-(void) onLcdShowMagicCustomDisplay: (BOOL)isSuccess;

-(void) onUpdatePosMagicFirmwareResult:(UpdateInformationResult)result;

-(void) onDownloadRsaPublicKeyResult:(NSDictionary *)result;
-(void) onPinKeyTDESMagicResult:(NSString *)encPin;
-(void) onGetPosCommMagic:(NSInteger)mode amount:(NSString *)amt posId:(NSString*)aPosId;

-(void) onUpdateMasterKeyMagicResult:(BOOL)isSuccess aDic:(NSDictionary *)resultDic;
-(void) onEmvICCExceptionMagicData: (NSString*)tlv;
-(void) onAsynResetPosStatus:(NSString *)isReset;
-(void) onReturnGetEncryptDataResult:(NSDictionary *)dict;
-(void) OnReturnAddKsnResult: (NSString *)tlv;

@end

@class ReceiveCommand;
@class BasePos;
@class VPosBluetoot;
@class Tracer;
@class StartEmvSession;
@class DoPollCard;
@class GetDeviceInfo;
@class WaitSetUpAmount;
@class Utils;
@class SendCommand;
@class ReceiveCommand;
@class MagicPOsCmdID;
@class MagicPosAudio;
@class SaveCustomParams;
@class VPosNormalBluetooth;
@class CBPeripheral;

@interface MPOSService : NSObject


@property (nonatomic,assign)NSInteger emvL2KernelStatus;
@property (nonatomic,strong)BasePos *pos;
@property (nonatomic,strong)StartEmvSession *dea;
@property (nonatomic, assign) EmvOption emvOption;
@property (nonatomic, assign) BOOL isPosExistFlag;
@property (nonatomic, assign) BOOL isTradeFlag;
@property (nonatomic, assign) NSInteger SelectEmvAppIndex;
@property (nonatomic,assign) BOOL automaticDisconnectFlag;
@property (nonatomic, readonly) id <MPOSServiceListener> CallBackDelegate;
@property (nonatomic, assign) BOOL posInputAmountFlag;
@property (nonatomic, assign) BOOL posDisplayAmountFlag;
@property (nonatomic, assign) BOOL isDebitOrCredit;
@property (nonatomic, assign) NSInteger onLineTime;
@property (nonatomic, assign) BOOL isQuickEMV;
@property (nonatomic, assign) BOOL isSaveLog;
@property (nonatomic, copy) NSString *batchID;
@property (nonatomic, copy) NSString *formatID;
@property (nonatomic, assign) BOOL amoutPoint;
@property (nonatomic, copy) NSString *amountPointStr;
@property (nonatomic, copy) NSString *isLogSavedStr;
@property (nonatomic, copy) NSString * panStatusStr;
@property (nonatomic, assign) NSInteger panStatus;
@property (nonatomic, assign) NSInteger encryptType;
@property (nonatomic, assign) NSInteger keyIndex;
@property (nonatomic, assign) NSInteger maxLen;
@property (nonatomic, copy) NSString * typeFace;
@property (nonatomic, copy) NSString * cardNo;
@property (nonatomic, copy) NSString * date;
@property (nonatomic, copy)NSString *sendOnLineTlv;
@property (nonatomic, assign) Byte transacType;
@property (nonatomic, assign) BOOL isReturnTotalCount;
@property (nonatomic, assign) BOOL isVeritasReturn;
@property (nonatomic, assign) BOOL isBuildPinFlag;
+(MPOSService *)sharedInstance;
-(void)setDelegate:(id<MPOSServiceListener>)aDelegate;
-(void)setQueue:(dispatch_queue_t)queue;
-(void)setPosType:(PosType) aPosType;
#pragma 更新IPEK
-(void)doUpdateIPEKOperationtracksn:(NSString *)trackksn
                          trackipek:(NSString *)trackipek
                trackipekCheckValue:(NSString *)trackipekCheckValue
                             emvksn:(NSString *)emvksn
                            emvipek:(NSString *)emvipek
                  emvipekcheckvalue:(NSString *)emvipekcheckvalue
                             pinksn:(NSString *)pinksn
                            pinipek:(NSString *)pinipek
                  pinipekcheckValue:(NSString *)pinipekcheckValue
                              block:(void(^)(BOOL isSuccess,NSString *stateStr))EMVBlock;

//设置时间
-(void) setOnlineTime:(NSInteger)aTime;
//读取升级密钥
-(void)getUpdateCheckValueBlock:(void(^)(BOOL isSuccess,NSString *stateStr))updateCheckValueBlock;
//设置关机时间
-(void)doSetShutDownTime:(NSString *)timeOut;
//更新休眠时间
-(void)doSetSleepModeTime:(NSString *)timeOut  block:(void(^)(BOOL isSuccess,NSString *stateStr))sleepModeBlock;

-(BOOL)getBluetoothState;
-(NSInteger) getOnLineTime;
-(BOOL) connectBT: (NSString *)bluetoothName;
-(void) disconnectBT;

-(void) doPollCard:(NSInteger) timeout;
-(void) doEmvApp: (EmvOption)aemvOption;
-(void) cancelSetAmount;
-(void) setAmount: (NSString *)aAmount aAmountDescribe:(NSString *)aAmountDescribe currency:(NSString *)currency transactionType:(TransactionType)transactionType;
-(void) selectEmvApp: (NSInteger)index;
-(void) cancelSelectEmvApp;
-(void) finalConfirm: (BOOL)isConfirmed;
-(void) sendOnlineProcessResult: (NSString *)tlv;
-(void) isServerConnected: (BOOL)isConnected;
-(void) sendTime: (NSString *)aterminalTime;
-(NSString *) getSdkVersion;
-(void) getMPosInfo;
-(void) getMPosId;
-(void) getRandomNumber;
-(void)getPin:(NSString *)aTransactionData;

//add set the sleep time 2019-03-25
-(void)setPosSleepTime:(NSInteger)sleepTime;

//add 2019-04-11
-(void)updateEmvConfig:(NSString *)emvAppCfg;


/////////////////////////////////////////////
-(void)udpateWorkKey:(NSString *)workKey workKeyCheckValue:(NSString *)workKeyCheck;

///////////////////////////////////////////
-(void)udpateWorkKey:(NSString *)updateKey;
-(void) setMasterKey:(NSString *)key  checkValue:(NSString *)chkValue;
-(void) setMasterKey:(NSString *)key  checkValue:(NSString *)chkValue keyIndex:(NSInteger) mKeyIndex;

-(void)startAudio;
-(void)stopAudio;
-(BOOL)isMposPresent;
- (NSString *)getApiVersion ;
- (NSString *)getApiBuildNumber;

- (void)sendPinEntryResult:(NSString *)pin;
- (void)cancelPinEntry;
- (void)bypassPinEntry;


//add 2019-04-30
-(BOOL)isIdle;

//add 2019-05-20
-(BOOL)isIsIssScriptRes;
-(void) setPosPresent:(BOOL) flag;

-(void) doPollCard:(NSInteger)keyIndex delays:(NSInteger)timeout;



-(void)setCardTradeMode:(CardTradeMode) aCardTMode;
-(BOOL)mposStatus;
//获取POS连接状态
- (void)mposStatusTimeOut:(NSInteger)timeOut mposStatus:(void (^)(BOOL isSuccess, NSString *amountStr))mposStatusBlock;

-(void) getMPosId:(NSInteger)timeout;

-(NSInteger)getUpdateProgress;
-(void)updatePosFirmware:(NSData*)aData address:(NSString*)devAddress;

-(void) onRequestWaitingUser;
-(void) onMposIdResult: (NSDictionary*)posId;
-(void) onMposInfoResult: (NSDictionary*)posInfoData;
-(void) onDoPollCardResult: (DoPollCardResult)result DecodeData:(NSDictionary*)decodeData;
-(void) onRequestSetAmountMagic;
-(void) onRequestSelectEmvAppMagic: (NSArray*)appList;
-(void) onRequestIsServerConnectedMagic;
-(void) onRequestFinalConfirmMagic;
-(void) onRequestOnlineProcessMagic: (NSString*) tlv;
-(void) onRequestTransactionResult: (TransactionResult)transactionResult;
-(void) onRequestBatchData: (NSString*)tlv;
-(void) onRequestMposConnected;
-(void) onRequestMposDisconnected;
-(void) onRequestNoMposDetected;
-(void) onErrorMagic:(Error)errorState;
-(void) onRequestDisplayMagic: (Display)displayMsg;

-(void) onRequestUpdateWorkKeyMagicResult: (UpdateInformationResult)updateInformationResult;
-(void) onReturnReversalData: (NSString*)tlv;
-(void) onDoGetRandomNumberResult: (NSString *)randomNum;
//add 2019-04-11
-(void)onReturnCustomConfigResult:(BOOL)isSuccess config:(NSString*)resutl;

-(void) onRequestPinEntry;

-(void) onReturnSetMasterKeyResult: (BOOL)isSuccess;
-(void) onUpdatePosMagicFirmwareResult:(UpdateInformationResult)result;

-(void) onGetPosCommMagic:(NSInteger)mode amount:(NSString *)amt posId:(NSString*)aPosId;

-(void) onUpdateMasterKeyMagicResult:(BOOL)isSuccess aDic:(NSDictionary *)resultDic;

-(void) onEmvICCExceptionMagicData: (NSString*)tlv;
-(void) onReturnGetEncryptDataResult:(NSDictionary*)tlv;

- (void)getPin:(NSInteger)encryptType keyIndex:(NSInteger)keyIndex maxLen:(NSInteger)maxLen typeFace:(NSString *)typeFace cardNo:(NSString *)cardNo data:(NSString *)data delay:(NSInteger)timeout withResultBlock:(void (^)(BOOL isSuccess, NSDictionary * result))getPinBlock;

- (void)confirmAmount:(NSString *)wKey delay:(NSInteger)timeout withResultBlock:(void (^)(BOOL isSuccess))confirmAmountBlock;

-(void) setAmount: (NSString *)aAmount aAmountDescribe:(NSString *)aAmountDescribe currency:(NSString *)currency transactionType:(TransactionType)transactionType posDisplayAmount:(BOOL)flag;

-(BOOL) resetPosStatus;
-(BOOL) resetMPos;
-(NSDictionary *)getICCTag:(NSInteger)encType tagArrStr:(NSString*) mTagArrStr;
-(NSDictionary *)getNFCBatchData;
-(void)isCardExist:(NSInteger)timeout withResultBlock:(void (^)(BOOL))isCardExistBlock;
-(void)setBTAutoDetecting: (BOOL)flag;
-(BOOL) connectBluetoothNoScan: (NSString*)bluetoothName;
-(NSArray *)getConnectedDevices;

-(void)doUpdateIPEKOperation:(NSString *)groupKey
                     tracksn:(NSString *)trackksn
                   trackipek:(NSString *)trackipek
         trackipekCheckValue:(NSString *)trackipekCheckValue
                      emvksn:(NSString *)emvksn
                     emvipek:(NSString *)emvipek
           emvipekcheckvalue:(NSString *)emvipekcheckvalue
                      pinksn:(NSString *)pinksn
                     pinipek:(NSString *)pinipek
           pinipekcheckValue:(NSString *)pinipekcheckValue
                       block:(void(^)(BOOL isSuccess,NSString *stateStr))EMVBlock;

-(void)getEncryptData:(NSData *)data keyType:(NSString*)keyType keyIndex:(NSString *)keyIndex timeOut:(NSInteger)timeout;

@end

