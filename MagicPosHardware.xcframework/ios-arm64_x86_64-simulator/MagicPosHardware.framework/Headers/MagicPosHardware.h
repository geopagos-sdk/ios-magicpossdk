//
//  MagicPosHardware.h
//  MagicPosHardware
//
//  Created by Andres Ispani on 23/06/2020.
//  Copyright © 2020 GeoPagos. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MagicPosHardware.
FOUNDATION_EXPORT double MagicPosHardwareVersionNumber;

//! Project version string for MagicPosHardware.
FOUNDATION_EXPORT const unsigned char MagicPosHardwareVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MagicPosHardware/PublicHeader.h>


